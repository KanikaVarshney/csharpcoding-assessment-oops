﻿using System;

namespace ConsoleApp1
{
    

    public class Developer
    {
        public int Id;
        public string DeveloperName;
        public string JoiningDate;
        public string ProjectAssigned;

        public Developer()
        {

        }

        public Developer(int id, string developerName, string joiningDate, string projectAssigned)
        {
            this.Id = id;
            this.DeveloperName = developerName;
            this.JoiningDate = joiningDate;
            this.ProjectAssigned = projectAssigned;
        }

        public void SetDetails()
        {
            Console.WriteLine("Enter Id");
            Id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Name");
            DeveloperName = Console.ReadLine();
            Console.WriteLine("Enter Joining Date");
            JoiningDate = Console.ReadLine();
            Console.WriteLine("Enter Name of the assigned project");
            ProjectAssigned = Console.ReadLine();
        }
        public void GetDetails()
        {
            Console.WriteLine("Developer ID: " + Id);
            Console.WriteLine("Developer Name: " + DeveloperName);
            Console.WriteLine("Joining Date: " + JoiningDate);
            Console.WriteLine("Project Assigned: " + ProjectAssigned);
        }
    }

    public class OnContract : Developer
    {
        public int Duration;
        public double ChargesPerDay;
        public double NetSalary;

        public OnContract()
        {

        }
        public OnContract(int id, string name, string joiningDate, string projectAssigned, int duration, double chargesPerDay)
            : base(id, name, joiningDate, projectAssigned)
        {
            this.Duration = duration;
            this.ChargesPerDay = chargesPerDay;
        }


        public void SetDetails()
        {
            base.SetDetails();
            Console.WriteLine("Enter duration");
            Duration = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Charges per day");
            ChargesPerDay = Convert.ToDouble(Console.ReadLine());
            NetSalary = CalculateNetSalary();
        }

        public void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Duration: " + Duration + " days");
            Console.WriteLine("Charges per day: " + ChargesPerDay);
        }
        public double CalculateNetSalary()
        {
            double netsal = 0;
            netsal = Duration * ChargesPerDay;
            return netsal;
        }
    }

    public class OnPayroll : Developer
    {
        public string Dept;
        public string Manager;
        public double NetSalary, Basic;
        public int Exp;

        public double DA;
        public double HRA;
        public double LTA;
        public double PF;

        public OnPayroll()
        {

        }
        public OnPayroll(int id, string developerName, string joiningDate, string projectAssigned, string dept, string manager, double basic, int exp) : base(id, developerName, joiningDate, projectAssigned)
        {
            this.Dept = dept;
            this.Manager = manager;
            this.Basic = basic;
            this.Exp = exp;
        }
        public void SetDetails(string dept, string manager, double basic, int exp)
        {
            base.SetDetails();
            Dept = dept;
            Manager = manager;
            Basic = basic;
            Exp = exp;
            NetSalary = CalculateNetSalary();

        }

        public void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Department: " + Dept);
            Console.WriteLine("Manager: " + Manager);
            Console.WriteLine("Experience: " + Exp + " years");
            Console.WriteLine("Net Salary: " + NetSalary);
        }

        public double CalculateNetSalary()
        {
            double netsal = 0;
            DA = Basic * .2;
            HRA = Basic * .15;
            LTA = Basic * .17;
            PF = 1500;
            netsal = Basic + DA + HRA + LTA - PF;
            return netsal;
        }
    }
}